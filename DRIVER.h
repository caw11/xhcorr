
// out-of-the-box headers
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <cstdlib> 
#include <time.h>
#include "math.h"

/* Parameters */
#include "Cosmo/COSMO_PARAMS.h"
						 //#include "ANAL_PARAMS.H"
#include "USERFLAGS.h"

/* Class for cosmology calculations: */
#include "Cosmo/dcosmology.h"
/* Class for manipulating 21CMFAST output: */ 
#include "Reio/TOCMFAST.h" 
/* Class for calculating 3point statistics: */ 
#include "Stats/STAT3PT.h"
/* Class to access reionization analytical functions:*/
#include "Reio/REIONISE.h" 

/*****
 * SET-UP VARIOUS OUTPUT FILES
 ****/
FILE *DBUG;
char filename[500], outputdir[500];
FILE *Fname;

/***** 
 * PARAMETER SET-UP
 *****/

double VSN(1e-30); //a very small number!!!!

// GENERAL variables
float *box, L;
double ionf, r12;
int boxtype, dim;

//Time stamp variables
int timer_store(0);
char clocklabel[500];


//EXTERNAL FUNCTIONS
void time_ellapsed(FILE *LogFile, int timer, int timer_store, char *point_of_ref); // To print out the ellapsed times to the debug file

	

