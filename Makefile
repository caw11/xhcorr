##################### USAGE GUIDE ##########################

# make to compile the program
# Then to run on a single box:
# 	>> 	./run_BOOTSTRAP.x path2box

# alternatively to run on multiple boxes use make run option

################### 'make run' command:#######################
# Nb. To choose to use this option than you must:

# 1. make sure you have made a file containing 
#box names to be analysed, e.g.

#	>>	ls PATH/delta_T* > boxlist
# 2. Make sure their redshift orders match (although this will be flagged
#    by the program it is a problem)

# 3. Make sure the box variables in INIT_PARAMS from COSMOLOGY 
# match those of the 21CMFAST boxes you are analysing

# 4. define the FILE variable when you call. i.e. 
# 	>>	make run FILE=PATH/TargetFileName
###############################################################

CFLAGS = -g -Wall -Wno-deprecated -O 

LIB= -lfftw3f -lm -lgsl -lgslcblas

OBJ = NR/dnumrecipes.cc NR/spline.cc NR/spline2D.cc NR/dcomplex.cc Cosmo/dcosmology.cc Stats/stat3pt.cc Reio/tocmfast.cc Reio/reionise.cc 

CC = g++

# Instructions on how to treat various file suffixes:
.SUFFIXES : .o .cc
.cc.o :
		$(CC) $(CFLAGS) -c $<

# Generic 'make' rule to follow:
all: run_stat3pt.x

run_stat3pt.x : $(OBJ) driver.o
	$(CC) $(CFLAGS) -o $@ driver.o $(OBJ) $(LIB) $(MISC)

run : 
	bash makeRun.sh > $(FILE)

# 'make clean' command
clean :
	rm -f *.o *.x *~
