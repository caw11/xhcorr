#ifndef _USR_H
#define _USR_H


/**********************************************************
 ************** UNIVERSAL USER FLAGS ********************** 
 * IF YOU CHANGE OUGHT IN HERE YOU HAVE TO 
 * MAKE CLEAN BEFORE MAKING
 * OR YOUR EFFORTS WILL BE IGNORED ***********************************************************/

const float DEBUG (1);
/* DEBUG defines the level of output detail you wish to be provided in runtime
 *
   0 = print out NOTHING
   1 = print essential check points & core variables (MY FAVE!)
   2 = print out all variables & essential check points 
   3 = print out every fricking detail... But excluding derivative related routines.

*/

// END USER ADJUSTABLE VARIABLES : DO NOT ADJUST THE FOLLOWING:

#endif
