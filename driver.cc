/**************************************************************
driver.cc
 
 This models the analytical  3-point correlation function of 
the ionisation field. 
It approximates that the field is discrete i.e. either 0 or 1. 
Utilises the same approach as in FZH04 but for 3 points.

Also takes an simulation output (3D box indexed to 1D
, details in header file) and calculates its correlation
function.

USAGE:
./run_stat3pt.x [boxfile] [boxtype]

[boxfile]=valid path to a box to be analysed
[boxtype]=type of box    (1=xH,   2=dT   3=density)

***************************************************************/
#include "DRIVER.h"

using namespace std;

// Commence the main program
int main(int argc, char *argv[])
{
  /******************************************************************/
  /******************* !!!! INITIALISATION !!!! *********************/
  /******************************************************************/

  DBUG = fopen("DebugLog", "w");
  if (!DBUG){
    fprintf(stderr, "driver.cc: Unable to open log file\n Aborting...\n");
    return -1;
  }
  fprintf(DBUG, "Now initialising general parameters and classes.... \n");


  fprintf(DBUG, "\n-----Cosmological parameter values-----\n Omega0 = %3.2f \n Lambda0 = %3.2f \n Omegab = %5.3f \n h = %3.2f \n sigma(8Mpc/h) = %3.2f \n n = %3.1f \n OmegaNu = %3.1f \n",om0,lam0,omb,h,s8,n,omNu);

  sprintf(outputdir, "mkdir Output_files");
  system(outputdir);
  sprintf(outputdir, "Output_files");

  /*************** PREPARE POINTERS AND OUTFILES******************/

  // initialise the cosmoloy and Tocmfast classes
  Cosmology c(0.3,0.7,0.046,0.7,0.9,1.0,0); 
  //Cosmology c(om0,lam0,omb,h,s8,n,omNu); 
  Tocmfast T0(DBUG, &c);  
  boxtype=atof(argv[2]);
  T0.variXtract(argv[1], boxtype, dim, L);
  T0.setz(13.0); //overiding the box properties so that I can compare with fig 5. FZH04
  Reionise Reio(&c, &T0, T0.getz(), DBUG);


  fprintf(stderr,"boxtype=%i (1=xH, 2=dT, 3=delta)\n Box resolution per side is %i and its box length %.2f Mpc\n Redshift = %.2f, neutral fraction=%.5f\n",boxtype, dim, L, T0.getz(), T0.getnf());

  if ((dim<5) && (L<10))
    fprintf(stderr,"***************** ERROR**********************\n The values of resolution and dimension seem dodgy. Please check your boxname and boxtype match, if they do then your filename format must be different from that of 21CMFAST output that tocmfast.cc is designed to work with\n");
  if (T0.getz()<=0)
    fprintf(stderr,"***************** ERROR**********************\n Extracted redshift value is wrong. Please check your boxname and boxtype match, if they do then your filename format must be different from that of 21CMFAST output that tocmfast.cc is designed to work with\n");
  if (  !( (0.0<T0.getnf())&&(T0.getnf()<=1.0) )  )
    fprintf(stderr,"***************** ERROR**********************\n Extracted neutral fraction value is wrong. Please check your boxname and boxtype match, if they do then your filename format must be different from that of 21CMFAST output that tocmfast.cc is designed to work with\n");

  box =(float *) malloc(sizeof(float)*pow(dim,3));

  if (DEBUG>=1) {
    time_ellapsed(DBUG, clock(), timer_store, "Initialisation");
    timer_store = clock();
    fprintf(DBUG, "Now importing box... \n");
  }
  if (boxtype>2)
    T0.smoothbox(argv[1], box, 0.75, boxtype); 
  else {
    T0.importbox(argv[1], box, boxtype, dim);
  }


  if (DEBUG>=1) {
    time_ellapsed(DBUG, clock(), timer_store, "Box import");
    timer_store = clock();
    fprintf(DBUG, "Now initialising Stat3pt.... \n");
  }

  Stat3pt S21(DBUG, box, dim, L, boxtype, &T0, &Reio, &c);
  free(box); //for this application I do not need box anymore as it is now stored inside the S21 evoking of Stat3pt

  if (DEBUG>=1) {
    time_ellapsed(DBUG, clock(), timer_store, "Stat3pt class initialise");
    timer_store = clock();
  }


  
  Reio.setBtype(0);
  /*/ ADD* to exclude FZH nf calculations
  fprintf(stderr,"\nCalculating ionised fraction\n");
  double z=ZSTART;
  for (int i=0;i<NUMzBINS;i++) {
    Reio.setz(z);
    cout<<Reio.getz()<<"\t"<<Reio.Qbar()<<endl;
    z+=0.25;
  }//*/
  /*double dummy;
  double Mmin=coolMass(&c,T0.getz());
  double sigmaMmin=c.sigma0fM(Mmin, dummy,0);
  double K = inverf(1-pow(40,-1));
  double B0 = (c.delCrit0(T0.getz())/c.growthFac(T0.getz())) - sqrt(2)*K*sigmaMmin;
  cout<<B0<<endl;
    
  double Sig2=0.1;
  for (int i=0;i<NUMzBINS;i++) {
    cout<<sqrt(Sig2)<<"\t"<<Reio.Barrier(Sig2)<<endl;
    Sig2+=2;
  }

  double R(3.0),n;
  float prob, l;
  l=2.0;
  Mmin=-1;
  Reio.setA(5.3);
  //*/

  double dim[3] = {0.3,8.5,lawofcos(0.3,8.5,PI*0.9)}, corr;
  //cout<<S21.V3sph_Gib(10.0, 10.0, R, 9.5,8.5,PI*0.9)<<endl;
  //cout<<S21.V2sphere(6.0, 6.0,100.0)<<endl;
  //n=c.nCollObject(T0.getz(), Mmin);
  //ionf=ZETA*c.fCollPSExact(T0.getz(), -1);
  //ionf=( 1.0-exp(-Reio.Qbar()) );//*ZETA*c.fCollPSExact(T0.getz(), -1)/Reio.Qbar();

  ionf=Reio.Qbar();
  double inc (0.001), dec(0.01);
  cout<<"ionised fraction = "<<ionf<<endl;
  r12=0.001;
  for (int i=0; i<2;i++){
    dim[0] = r12; 
    //cout<<dim[0]<<"\t"<<dim[1]<<"\t"<<dim[2]<<endl;
  //cout<<"ionised fraction "<<n<<endl;
    corr=S21.P2pt(dim) - pow( ionf ,2.0);
    cout<<r12<<"\t"<<corr<<endl;
    if ((r12-dec)>=0.0){
      dec*=10;
      inc*=10;
      cout<<dec<<"\t"<<inc<<endl;
    }
    r12+=inc;
  }
  //cout<<"correlation fn for z = "<<T0.getz()<<" and separation = "<<dim[0]<<"\t="<<corr<<endl;

  //cout<<"prob at least one source in sphere of size "<<R<<" is "<<S21.Pmin1(4.0/3.0*PI*pow(R,3))<<"the volume is"<<4.0/3.0*PI*pow(R,3)<<endl;
  //l=2.0*R*cos(PI/6.0);

  //prob=S21.PallIon(l,l,PI/3.0,n,R);
  //l=0.00001;
  //cout<<prob<<endl; //this is bull
  // prob=S21.P2pt(l,n,R);
  //cout<<prob<<endl;

} // THE END!!!

//EXTERNAL FUNCTIONS
// time_ellapsed -> To print out the ellapsed times to the debug file
void time_ellapsed(FILE *LogFile, int timer, int timer_store, char *point_of_ref)
{
  fprintf(LogFile, "%s complete, running time = %f, time taken by process = %f\n", point_of_ref, ((float)timer)/CLOCKS_PER_SEC,((float)(timer-timer_store))/CLOCKS_PER_SEC);
}


